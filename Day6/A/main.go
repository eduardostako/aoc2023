package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	mulWays := 1
	tuple := readInput()
	for time, distance := range tuple {
		var ways int
		for i := 0; i < time/2+1; i++ {
			if i*(time-i) > distance {
				ways++
			}
		}
		fmt.Println(ways)
		if time%2 == 0 {
			mulWays *= ((ways - 1) * 2) + 1
		} else {
			mulWays *= ways * 2
		}
	}

	fmt.Println(mulWays)
}

func readInput() map[int]int {
	file, err := os.Open("../input")
	var times []string = make([]string, 0)
	var distances []string = make([]string, 0)
	var tuple map[int]int = make(map[int]int)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		if strings.Contains(line, "Time") {
			times = strings.Split(strings.Split(line, ":")[1], " ")
		}
		if strings.Contains(line, "Distance") {
			distances = strings.Split(strings.Split(line, ":")[1], " ")
		}
	}
	for i, val := range times {
		tuple[atoi(val)] = atoi(distances[i])
	}
	return tuple
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
