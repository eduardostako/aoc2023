package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	var sum int
	oasisReadings := readInput()
	for _, oasisReading := range oasisReadings {
		var intermidateSteps map[int][]int = make(map[int][]int)
		var step int
		var allZeroes bool
		intermidateSteps[step] = oasisReading
		for !allZeroes {
			intermidateSteps[step+1] = getNewStep(intermidateSteps[step])
			allZeroes = true
			for _, value := range intermidateSteps[step+1] {
				if value != 0 {
					allZeroes = false
					break
				}
			}
			step++
		}
		var newValue int
		for i := len(intermidateSteps) - 1; i > 0; i-- {
			intermidateSteps[i] = append([]int{newValue}, intermidateSteps[i]...)
			newValue = intermidateSteps[i-1][0] - intermidateSteps[i][0]
		}
		sum += newValue
	}
	fmt.Println(sum)
}

func getNewStep(step []int) (newStep []int) {
	for i := 0; i < len(step)-1; i++ {
		newStep = append(newStep, step[i+1]-step[i])
	}
	return
}

func readInput() (oasisReadings [][]int) {
	file, err := os.Open("../input")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		oasisReadings = append(oasisReadings, sliceAtoi(strings.Split(line, " ")))
	}
	return
}

func sliceAtoi(input []string) (output []int) {
	for _, value := range input {
		i, err := strconv.Atoi(value)
		if err != nil {
			log.Fatal(err)
		} else {
			output = append(output, i)
		}
	}
	return
}
