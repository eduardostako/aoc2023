package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	directions, nodes := readInput()
	initialNodes := getListInitialNodes(nodes)
	var periods []int = make([]int, 0)
	var steps int
	for len(periods) != len(initialNodes) {
		for _, direction := range directions {
			steps++
			for i, node := range initialNodes {
				if direction == 'L' {
					initialNodes[i] = nodes[node][0]
				} else {
					initialNodes[i] = nodes[node][1]
				}
			}
			for _, node := range initialNodes {
				if node[2] == 'Z' {
					periods = append(periods, steps)
				}
			}
		}
	}
	fmt.Println(LCM(periods...))
}

// greatest common divisor (GCD) via Euclidean algorithm
func GCD(a, b int) int {
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

// find Least Common Multiple (LCM) via GCD
func LCM(integers ...int) int {
	result := integers[0] * integers[1] / GCD(integers[0], integers[1])

	for i := 2; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}

	return result
}

func getListInitialNodes(nodes map[string][2]string) (initialNodes []string) {
	for node := range nodes {
		if node[2] == 'A' {
			initialNodes = append(initialNodes, node)
		}
	}
	return
}

func readInput() (string, map[string][2]string) {
	file, err := os.Open("../input")
	var nodes map[string][2]string = make(map[string][2]string)
	var directions string
	r := regexp.MustCompile(`(?P<Node>[A-Z]{3}) = \((?P<Left>[A-Z]{3}), (?P<Right>[A-Z]{3})\)`)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		matches := r.FindStringSubmatch(line)
		if len(matches) == 0 && len(line) != 0 {
			directions = line
		} else if len(matches) != 0 {
			nodes[matches[r.SubexpIndex("Node")]] = [2]string{matches[r.SubexpIndex("Left")], matches[r.SubexpIndex("Right")]}
		}
	}
	return directions, nodes
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
