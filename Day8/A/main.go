package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

func main() {
	directions, nodes := readInput()
	fmt.Println(directions)
	fmt.Println(nodes)
	actualNode := "AAA"
	var steps int
	for actualNode != "ZZZ" {
		for _, direction := range directions {
			steps++
			if direction == 'L' {
				actualNode = nodes[actualNode][0]
			} else {
				actualNode = nodes[actualNode][1]
			}
			if actualNode == "ZZZ" {
				break
			}
			fmt.Println(actualNode)

		}
	}
	fmt.Println(actualNode)
	fmt.Println(steps)
}

func readInput() (string, map[string][2]string) {
	file, err := os.Open("../input")
	var nodes map[string][2]string = make(map[string][2]string)
	var directions string
	r := regexp.MustCompile(`(?P<Node>[A-Z]{3}) = \((?P<Left>[A-Z]{3}), (?P<Right>[A-Z]{3})\)`)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		matches := r.FindStringSubmatch(line)
		if len(matches) == 0 && len(line) != 0 {
			directions = line
		} else if len(matches) != 0 {
			nodes[matches[r.SubexpIndex("Node")]] = [2]string{matches[r.SubexpIndex("Left")], matches[r.SubexpIndex("Right")]}
		}
	}
	return directions, nodes
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
