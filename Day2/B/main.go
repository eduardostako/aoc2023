package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Turn struct {
	red, green, blue int
}

type Game struct {
	gameNumber int
	turns      []Turn
}

func main() {
	var sum int
	for _, game := range readInput() {
		sum += game.checkMin()
	}
	fmt.Println(sum)
}

func (g *Game) checkMin() int {
	var minGreen, minRed, minBlue int
	for _, turn := range g.turns {
		if turn.green > minGreen {
			minGreen = turn.green
		}
		if turn.red > minRed {
			minRed = turn.red
		}
		if turn.blue > minBlue {
			minBlue = turn.blue
		}
	}
	return minBlue * minRed * minGreen
}

func readInput() []Game {
	var lines []Game

	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		game := strings.Split(line, ":")
		var processedGame Game
		var turns []Turn
		processedGame.gameNumber = atoi(strings.Split(game[0], " ")[1])
		processedGame.turns = turns
		for _, turn := range strings.Split(game[1], ";") {
			var processedTurn Turn
			for _, gem := range strings.Split(turn, ",") {
				regex := regexp.MustCompile(`[0-9]{1,2}`)
				res := regex.FindStringSubmatch(gem)
				if strings.Contains(gem, "red") {
					processedTurn.red = atoi(res[0])
				} else if strings.Contains(gem, "green") {
					processedTurn.green = atoi(res[0])
				} else {
					processedTurn.blue = atoi(res[0])
				}
			}
			processedGame.turns = append(processedGame.turns, processedTurn)
		}
		lines = append(lines, processedGame)
	}
	return lines
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
