package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

var sb strings.Builder

var one = [3]rune{'o', 'n', 'e'}
var two = [3]rune{'t', 'w', 'o'}
var thr = [3]rune{'t', 'h', 'r'}
var fou = [3]rune{'f', 'o', 'u'}
var fiv = [3]rune{'f', 'i', 'v'}
var six = [3]rune{'s', 'i', 'x'}
var sev = [3]rune{'s', 'e', 'v'}
var eig = [3]rune{'e', 'i', 'g'}
var nin = [3]rune{'n', 'i', 'n'}

func main() {
	var sum int
	for _, line := range readInput() {
		var first, last rune
		for i, char := range line {
			if unicode.IsNumber(char) {
				if first == 0 {
					first = char
					last = char
				} else {
					last = char
				}
			} else {
				if len(line) > i+2 {
					if char == one[0] && rune(line[i+1]) == one[1] && rune(line[i+2]) == one[2] {
						if first == 0 {
							first = '1'
							last = '1'
						} else {
							last = '1'
						}
					} else if char == two[0] && rune(line[i+1]) == two[1] && rune(line[i+2]) == two[2] {
						if first == 0 {
							first = '2'
							last = '2'
						} else {
							last = '2'
						}
					} else if char == thr[0] && rune(line[i+1]) == thr[1] && rune(line[i+2]) == thr[2] {
						if first == 0 {
							first = '3'
							last = '3'
						} else {
							last = '3'
						}
					} else if char == fou[0] && rune(line[i+1]) == fou[1] && rune(line[i+2]) == fou[2] {
						if first == 0 {
							first = '4'
							last = '4'
						} else {
							last = '4'
						}
					} else if char == fiv[0] && rune(line[i+1]) == fiv[1] && rune(line[i+2]) == fiv[2] {
						if first == 0 {
							first = '5'
							last = '5'
						} else {
							last = '5'
						}
					} else if char == six[0] && rune(line[i+1]) == six[1] && rune(line[i+2]) == six[2] {
						if first == 0 {
							first = '6'
							last = '6'
						} else {
							last = '6'
						}
					} else if char == sev[0] && rune(line[i+1]) == sev[1] && rune(line[i+2]) == sev[2] {
						if first == 0 {
							first = '7'
							last = '7'
						} else {
							last = '7'
						}
					} else if char == eig[0] && rune(line[i+1]) == eig[1] && rune(line[i+2]) == eig[2] {
						if first == 0 {
							first = '8'
							last = '8'
						} else {
							last = '8'
						}
					} else if char == nin[0] && rune(line[i+1]) == nin[1] && rune(line[i+2]) == nin[2] {
						if first == 0 {
							first = '9'
							last = '9'
						} else {
							last = '9'
						}
					}
				}
			}
		}
		sb.WriteRune(first)
		sb.WriteRune(last)
		fmt.Println(sb.String())
		sum += atoi(sb.String())
		sb.Reset()
	}
	fmt.Println(sum)
}

func readInput() []string {
	var lines []string

	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		lines = append(lines, line)
	}
	return lines
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
