package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Beam struct {
	x, y int
	dir  string
}

type Pos struct {
	x, y int
}

func transformBeam(tiles []string, activeBeams []Beam, i int, visited map[Beam]bool) ([]Beam, map[Beam]bool) {
	switch tile := tiles[activeBeams[i].x][activeBeams[i].y]; tile {
	case '/':
		switch activeBeams[i].dir {
		case "R":
			activeBeams[i].dir = "U"
		case "L":
			activeBeams[i].dir = "D"
		case "U":
			activeBeams[i].dir = "R"
		case "D":
			activeBeams[i].dir = "L"
		}
	case '\\':
		switch activeBeams[i].dir {
		case "R":
			activeBeams[i].dir = "D"
		case "L":
			activeBeams[i].dir = "U"
		case "U":
			activeBeams[i].dir = "L"
		case "D":
			activeBeams[i].dir = "R"
		}
	case '|':
		if activeBeams[i].dir == "L" || activeBeams[i].dir == "R" {
			activeBeams[i].dir = "U"
			activeBeams = append(activeBeams, Beam{activeBeams[i].x, activeBeams[i].y, "D"})
		}
	case '-':
		if activeBeams[i].dir == "U" || activeBeams[i].dir == "D" {
			activeBeams[i].dir = "R"
			activeBeams = append(activeBeams, Beam{activeBeams[i].x, activeBeams[i].y, "L"})
		}
	}

	switch activeBeams[i].dir {
	case "R":
		activeBeams[i].y++
	case "L":
		activeBeams[i].y--
	case "U":
		activeBeams[i].x--
	case "D":
		activeBeams[i].x++
	}

	if visited[activeBeams[i]] || activeBeams[i].x < 0 || activeBeams[i].x >= len(tiles) || activeBeams[i].y < 0 || activeBeams[i].y >= len(tiles[activeBeams[i].x]) {
		s := activeBeams
		s = append(s[:i], s[i+1:]...)
		activeBeams = s
	}

	if len(activeBeams) != 0 && !visited[activeBeams[i]] {
		visited[activeBeams[i]] = true
	}

	return activeBeams, visited

}

func main() {
	var max int
	tiles := readInput()
	//LEFT
	for i := range tiles {
		activeBeams := []Beam{{i, 0, "R"}}
		var visited map[Beam]bool = make(map[Beam]bool)
		visited[activeBeams[0]] = true
		var visitedPos map[Pos]bool = make(map[Pos]bool)
		for len(activeBeams) != 0 {
			activeBeams, visited = transformBeam(tiles, activeBeams, 0, visited)
		}
		for key := range visited {
			visitedPos[Pos{key.x, key.y}] = true
		}
		if len(visitedPos) > max {
			max = len(visitedPos)
		}
	}
	//RIGHT
	for i := range tiles {
		activeBeams := []Beam{{i, len(tiles) - 1, "L"}}
		var visited map[Beam]bool = make(map[Beam]bool)
		var visitedPos map[Pos]bool = make(map[Pos]bool)
		visited[activeBeams[0]] = true
		for len(activeBeams) != 0 {
			activeBeams, visited = transformBeam(tiles, activeBeams, 0, visited)
		}
		for key := range visited {
			visitedPos[Pos{key.x, key.y}] = true
		}
		if len(visitedPos) > max {
			max = len(visitedPos)
		}
	}
	//TOP
	for i := range tiles {
		activeBeams := []Beam{{0, i, "D"}}
		var visited map[Beam]bool = make(map[Beam]bool)
		var visitedPos map[Pos]bool = make(map[Pos]bool)
		visited[activeBeams[0]] = true
		for len(activeBeams) != 0 {
			activeBeams, visited = transformBeam(tiles, activeBeams, 0, visited)
		}
		for key := range visited {
			visitedPos[Pos{key.x, key.y}] = true
		}
		if len(visitedPos) > max {
			max = len(visitedPos)
		}
	}
	//BOTTOM
	for i := range tiles {
		activeBeams := []Beam{{len(tiles) - 1, i, "U"}}
		var visited map[Beam]bool = make(map[Beam]bool)
		var visitedPos map[Pos]bool = make(map[Pos]bool)
		visited[activeBeams[0]] = true
		for len(activeBeams) != 0 {
			activeBeams, visited = transformBeam(tiles, activeBeams, 0, visited)
		}
		for key := range visited {
			visitedPos[Pos{key.x, key.y}] = true
		}
		if len(visitedPos) > max {
			max = len(visitedPos)
		}
	}

	fmt.Println(max)
}

func readInput() (tiles []string) {
	file, err := os.Open("../input")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		tiles = append(tiles, line)
	}
	return
}
