package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Card struct {
	winningNumbers, numbers []string
}

func main() {
	var sum int
	var cardRegistering int
	var winnerCard, registeredCard map[int]int
	winnerCard = make(map[int]int)
	registeredCard = make(map[int]int)
	cards := readInput()

	for key := range cards {
		registeredCard[key] = 1
	}

	for len(registeredCard) > 0 {
		cardRegistering++
		score := cards[cardRegistering].getScore()
		for i := 0; i < registeredCard[cardRegistering]; i++ {
			for j := cardRegistering + 1; j < cardRegistering+score+1; j++ {
				registeredCard[j]++
			}
		}
		winnerCard[cardRegistering] = registeredCard[cardRegistering]
		delete(registeredCard, cardRegistering)
	}

	for _, value := range winnerCard {
		sum += value
	}
	fmt.Println(sum)
}

func (c Card) getScore() int {
	return len(intersection(c.winningNumbers, c.numbers))
}

func intersection(s1, s2 []string) (inter []string) {
	hash := make(map[string]bool)
	for _, e := range s1 {
		hash[e] = true
	}
	for _, e := range s2 {
		if hash[e] {
			inter = append(inter, e)
		}
	}
	return
}

func readInput() map[int]Card {
	var cards = make(map[int]Card)

	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		card := strings.Split(line, ":")
		var processedCard Card
		cardNumber := atoi(strings.Split(card[0], " ")[1])
		numbers := strings.Split(card[1], "|")
		processedCard.winningNumbers = strings.Split(numbers[0], " ")
		processedCard.numbers = strings.Split(numbers[1], " ")

		cards[cardNumber] = processedCard
	}
	return cards
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
