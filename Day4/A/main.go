package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type Card struct {
	cardNumber              string
	winningNumbers, numbers []string
}

func main() {
	var sum float64
	cards := readInput()
	for _, card := range cards {
		sum += card.getScore()
	}
	fmt.Println(sum)
}

func (c *Card) getScore() float64 {
	score := len(intersection(c.winningNumbers, c.numbers))
	if score > 0 {
		return math.Pow(2, float64(score-1))
	}
	return 0
}

func intersection(s1, s2 []string) (inter []string) {
	hash := make(map[string]bool)
	for _, e := range s1 {
		hash[e] = true
	}
	for _, e := range s2 {
		if hash[e] {
			inter = append(inter, e)
		}
	}

	return
}

func readInput() []Card {
	var lines []Card

	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		card := strings.Split(line, ":")
		var processedCard Card
		processedCard.cardNumber = strings.Split(card[0], " ")[1]
		numbers := strings.Split(card[1], "|")
		processedCard.winningNumbers = strings.Split(numbers[0], " ")
		processedCard.numbers = strings.Split(numbers[1], " ")

		lines = append(lines, processedCard)
	}
	return lines
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
