package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type pos struct {
	x, y int
}

func (p *pos) sumPos(other pos) pos {
	return pos{p.x + other.x, p.y + other.y}
}

type mov struct {
	dir    pos
	origin string
}

var eastMov = map[rune]mov{
	'-': {
		pos{0, -1},
		"E",
	},
	'L': {
		pos{-1, 0},
		"S",
	},
	'F': {
		pos{1, 0},
		"N",
	},
}

var westMov = map[rune]mov{
	'-': {
		pos{0, 1},
		"W",
	},
	'J': {
		pos{-1, 0},
		"S",
	},
	'7': {
		pos{1, 0},
		"N",
	},
}

var southMov = map[rune]mov{
	'|': {
		pos{-1, 0},
		"S",
	},
	'7': {
		pos{0, -1},
		"E",
	},
	'F': {
		pos{0, 1},
		"W",
	},
}

var northMov = map[rune]mov{
	'|': {
		pos{1, 0},
		"N",
	},
	'L': {
		pos{0, 1},
		"W",
	},
	'J': {
		pos{0, -1},
		"E",
	},
}

func main() {
	pipe := readInput()
	var initialPos pos
	var mainLoop map[pos]bool = make(map[pos]bool)

	for i, pipeline := range pipe {
		for j, tile := range pipeline {
			if tile == 'S' {
				initialPos = pos{i, j}
				mainLoop[initialPos] = true
				break
			}
		}
	}
	//Find next turn after initial
	var nextMove pos
	var currentDir string
	if initialPos.x > 0 && pipe[initialPos.x-1][initialPos.y] != '.' && pipe[initialPos.x-1][initialPos.y] != 'L' && pipe[initialPos.x-1][initialPos.y] != 'J' {
		nextMove = initialPos.sumPos(pos{-1, 0})
		currentDir = "S"
	} else if initialPos.x+1 < len(pipe) && pipe[initialPos.x+1][initialPos.y] != '.' && pipe[initialPos.x+1][initialPos.y] != 'F' && pipe[initialPos.x+1][initialPos.y] != '7' {
		nextMove = initialPos.sumPos(pos{1, 0})
		currentDir = "N"
	} else if initialPos.y > 0 && pipe[initialPos.x][initialPos.y-1] != '.' && pipe[initialPos.x][initialPos.y-1] != 'J' && pipe[initialPos.x][initialPos.y-1] != 'F' {
		nextMove = initialPos.sumPos(pos{0, -1})
		currentDir = "E"
	} else if initialPos.y+1 < len(pipe) && pipe[initialPos.x][initialPos.y+1] != '.' && pipe[initialPos.x][initialPos.y+1] != 'F' && pipe[initialPos.x-1][initialPos.y+1] != 'L' {
		nextMove = initialPos.sumPos(pos{0, 1})
		currentDir = "W"
	}
	mainLoop[nextMove] = true

	for pipe[nextMove.x][nextMove.y] != 'S' {
		var movement mov
		if currentDir == "E" {
			movement = eastMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "W" {
			movement = westMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "N" {
			movement = northMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "S" {
			movement = southMov[pipe[nextMove.x][nextMove.y]]
		}
		nextMove = nextMove.sumPos(movement.dir)
		currentDir = movement.origin
		mainLoop[nextMove] = true
	}
	pipe[initialPos.x][initialPos.y] = '|'
	count := 0
	for i, pipeline := range pipe {
		inside := false
		lastStart := '-'
		for j, c := range pipeline {
			if !mainLoop[pos{i, j}] {
				if inside {
					count++
				}
				continue
			}
			if c == '-' {
				continue
			}
			if c == '|' || c == 'F' || c == 'L' {
				inside = !inside
				lastStart = c
			}
			if c == 'J' && lastStart == 'L' {
				inside = !inside
				lastStart = '-'
			}
			if c == '7' && lastStart == 'F' {
				inside = !inside
				lastStart = '-'
			}
		}
	}

	fmt.Println(count)
}

func readInput() (pipe [][]rune) {
	file, err := os.Open("../input")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		pipe = append(pipe, []rune(line))
	}
	return
}
