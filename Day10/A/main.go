package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type pos struct {
	x, y int
}

func (p *pos) sumPos(other pos) pos {
	return pos{p.x + other.x, p.y + other.y}
}

type mov struct {
	dir    pos
	origin string
}

var eastMov = map[rune]mov{
	'-': {
		pos{0, -1},
		"E",
	},
	'L': {
		pos{-1, 0},
		"S",
	},
	'F': {
		pos{1, 0},
		"N",
	},
}

var westMov = map[rune]mov{
	'-': {
		pos{0, 1},
		"W",
	},
	'J': {
		pos{-1, 0},
		"S",
	},
	'7': {
		pos{1, 0},
		"N",
	},
}

var southMov = map[rune]mov{
	'|': {
		pos{-1, 0},
		"S",
	},
	'7': {
		pos{0, -1},
		"E",
	},
	'F': {
		pos{0, 1},
		"W",
	},
}

var northMov = map[rune]mov{
	'|': {
		pos{1, 0},
		"N",
	},
	'L': {
		pos{0, 1},
		"W",
	},
	'J': {
		pos{0, -1},
		"E",
	},
}

func main() {
	pipe := readInput()
	var initialPos pos
	for i, pipeline := range pipe {
		for j, tile := range pipeline {
			if tile == 'S' {
				initialPos = pos{i, j}
				break
			}
		}
	}
	//Find next turn after initial
	var nextMove pos
	var currentDir string
	if initialPos.x > 0 && pipe[initialPos.x-1][initialPos.y] != '.' && pipe[initialPos.x-1][initialPos.y] != 'L' && pipe[initialPos.x-1][initialPos.y] != 'J' {
		nextMove = initialPos.sumPos(pos{-1, 0})
		currentDir = "S"
	} else if initialPos.x+1 < len(pipe) && pipe[initialPos.x+1][initialPos.y] != '.' && pipe[initialPos.x-1][initialPos.y] != 'F' && pipe[initialPos.x-1][initialPos.y] != '7' {
		nextMove = initialPos.sumPos(pos{1, 0})
		currentDir = "N"
	} else if initialPos.y > 0 && pipe[initialPos.x][initialPos.y-1] != '.' && pipe[initialPos.x-1][initialPos.y] != 'J' && pipe[initialPos.x-1][initialPos.y] != 'F' {
		nextMove = initialPos.sumPos(pos{0, -1})
		currentDir = "E"
	} else if initialPos.y+1 < len(pipe) && pipe[initialPos.x][initialPos.y+1] != '.' && pipe[initialPos.x-1][initialPos.y] != 'F' && pipe[initialPos.x-1][initialPos.y] != 'L' {
		nextMove = initialPos.sumPos(pos{0, 1})
		currentDir = "W"
	}

	var steps = 1
	for pipe[nextMove.x][nextMove.y] != 'S' {
		steps++
		var movement mov
		if currentDir == "E" {
			movement = eastMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "W" {
			movement = westMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "N" {
			movement = northMov[pipe[nextMove.x][nextMove.y]]
		} else if currentDir == "S" {
			movement = southMov[pipe[nextMove.x][nextMove.y]]
		}
		nextMove = nextMove.sumPos(movement.dir)
		currentDir = movement.origin
	}
	fmt.Println(steps / 2)
}

func readInput() (pipe [][]rune) {
	file, err := os.Open("../input7")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		pipe = append(pipe, []rune(line))
	}
	return
}
