package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	var sum int
	commands := readInput()

	for _, command := range commands {
		var currentValue int
		for _, letter := range command {
			currentValue += int(letter)
			currentValue *= 17
			currentValue %= 256
		}
		sum += currentValue
	}

	fmt.Println(sum)
}

func readInput() (commands []string) {
	file, err := os.Open("../input")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		commands = strings.Split(line, ",")
	}
	return
}
