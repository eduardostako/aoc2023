package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var sb strings.Builder

type Lens struct {
	label       string
	focalLength string
}

func main() {
	var sum int
	var lightBoxes map[int][]Lens = make(map[int][]Lens)
	commands := readInput()

	for _, command := range commands {
		var currentValue int
		for i, letter := range command {
			if letter == '-' {
				for j, lens := range lightBoxes[currentValue] {
					if lens.label == sb.String() {
						lightBoxes[currentValue] = remove(lightBoxes[currentValue], j)
					}
				}

			} else if letter == '=' {
				var found bool
				for j, lens := range lightBoxes[currentValue] {
					if lens.label == sb.String() {
						lightBoxes[currentValue][j] = Lens{sb.String(), string(command[i+1])}
						found = true
					}
				}
				if !found {
					lightBoxes[currentValue] = append(lightBoxes[currentValue], Lens{sb.String(), string(command[i+1])})
				}
			} else {
				currentValue += int(letter)
				currentValue *= 17
				currentValue %= 256
				sb.WriteRune(letter)
			}

		}
		sb.Reset()
	}

	for number, box := range lightBoxes {
		for i, lens := range box {
			sum += (number + 1) * atoi(lens.focalLength) * (i + 1)
		}
	}
	fmt.Println(lightBoxes)
	fmt.Println(sum)
}

func remove(slice []Lens, s int) []Lens {
	return append(slice[:s], slice[s+1:]...)
}

func readInput() (commands []string) {
	file, err := os.Open("../input")

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		commands = strings.Split(line, ",")
	}
	return
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
