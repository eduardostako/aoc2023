package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

var sb strings.Builder

type pos struct {
	x, y int
}

type numberPos struct {
	partNumber          int
	startPos, finishPos pos
}

var gears map[pos][]int

func main() {
	var sum int
	var parts []numberPos
	schematic := readInput()
	for i, line := range schematic {
		var counting bool
		var firstPos pos
		for j, char := range line {
			if unicode.IsNumber(char) {
				if !counting {
					firstPos = pos{i, j}
					sb.WriteRune(char)
					counting = true
				} else {
					sb.WriteRune(char)
				}
			} else {
				if counting {
					parts = append(parts, numberPos{partNumber: atoi(sb.String()), startPos: firstPos, finishPos: pos{i, j - 1}})
					sb.Reset()
					counting = false
				}
			}
		}
		if counting {
			parts = append(parts, numberPos{partNumber: atoi(sb.String()), startPos: firstPos, finishPos: pos{i, len(line) - 1}})
			sb.Reset()
		}
	}
	gears := make(map[pos][]int)
	for _, part := range parts {
		var min, max int
		if part.startPos.y-1 < 0 {
			min = 0
		} else {
			min = part.startPos.y - 1
		}
		if part.finishPos.y+1 >= len(schematic[part.startPos.x]) {
			max = len(schematic[part.startPos.x])
		} else {
			max = part.finishPos.y + 2
		}
		if part.startPos.x > 0 {
			for i, cell := range schematic[part.startPos.x-1][min:max] {
				if cell == '*' {
					gearPos := pos{part.startPos.x - 1, min + i}
					if _, ok := gears[gearPos]; ok {
						gears[gearPos] = append(gears[gearPos], part.partNumber)
					} else {
						gears[gearPos] = []int{part.partNumber}
					}
					break
				}

			}
		}
		if part.startPos.y > 0 {
			if schematic[part.startPos.x][part.startPos.y-1] == '*' {
				gearPos := pos{part.startPos.x, part.startPos.y - 1}
				if _, ok := gears[gearPos]; ok {
					gears[gearPos] = append(gears[gearPos], part.partNumber)
				} else {
					gears[gearPos] = []int{part.partNumber}
				}
			}
		}
		if part.finishPos.y+1 < len(schematic[part.startPos.x]) {
			if schematic[part.startPos.x][part.finishPos.y+1] == '*' {
				gearPos := pos{part.startPos.x, part.finishPos.y + 1}
				if _, ok := gears[gearPos]; ok {
					gears[gearPos] = append(gears[gearPos], part.partNumber)
				} else {
					gears[gearPos] = []int{part.partNumber}
				}
			}
		}

		if part.startPos.x+1 < len(schematic) {
			for i, cell := range schematic[part.startPos.x+1][min:max] {
				if cell == '*' {
					gearPos := pos{part.startPos.x + 1, min + i}
					if _, ok := gears[gearPos]; ok {
						gears[gearPos] = append(gears[gearPos], part.partNumber)
					} else {
						gears[gearPos] = []int{part.partNumber}
					}
					break
				}

			}
		}
	}

	for _, gearValues := range gears {
		if len(gearValues) > 1 {
			mult := 1
			for _, gearValue := range gearValues {
				mult *= gearValue
			}
			sum += mult
		}
	}

	fmt.Println(sum)
}

func readInput() [][]rune {
	var lines [][]rune

	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		lines = append(lines, []rune(line))
	}
	return lines
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
