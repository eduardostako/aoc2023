package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type conversion struct {
	source, dest, ranges int
}

var seed2Soil []conversion = make([]conversion, 0)
var soil2Fert []conversion = make([]conversion, 0)
var fert2Water []conversion = make([]conversion, 0)
var water2Light []conversion = make([]conversion, 0)
var light2Temp []conversion = make([]conversion, 0)
var temp2Hum []conversion = make([]conversion, 0)
var hum2Loc []conversion = make([]conversion, 0)
var seeds []string = make([]string, 0)

func main() {
	minLoc := math.MaxInt
	readInput()
	for i := 0; i < len(seeds); i += 2 {
		for j := 0; j < atoi(seeds[i+1]); j++ {

			//fmt.Printf("Seed %d ", atoi(seeds[i])+j)
			current := atoi(seeds[i]) + j
			for _, conv := range seed2Soil {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to soil %d, ", current)
			for _, conv := range soil2Fert {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to fert %d, ", current)
			for _, conv := range fert2Water {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to water %d, ", current)
			for _, conv := range water2Light {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to light %d, ", current)
			for _, conv := range light2Temp {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to temp %d, ", current)
			for _, conv := range temp2Hum {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to humidity %d, ", current)
			for _, conv := range hum2Loc {
				if current >= conv.source && current < conv.source+conv.ranges {
					current = conv.dest + (current - conv.source)
					break
				}
			}
			//fmt.Printf("to location %d ", current)
			//fmt.Println()
			if current < minLoc {
				minLoc = current
			}
		}
	}
	fmt.Printf("Min location is %d", minLoc)
}

func readInput() {
	file, err := os.Open("../input")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)
	for scan.Scan() {
		line := scan.Text()
		if strings.Contains(line, "seeds:") {
			seeds = append(seeds,
				strings.Split(
					strings.Trim(
						strings.Split(line, ":")[1], " "), " ")...)
		}
		if strings.Contains(line, "seed-to-soil map:") {
			extract(line, scan, &seed2Soil)
		}
		if strings.Contains(line, "soil-to-fertilizer map:") {
			extract(line, scan, &soil2Fert)
		}
		if strings.Contains(line, "fertilizer-to-water map:") {
			extract(line, scan, &fert2Water)
		}
		if strings.Contains(line, "water-to-light map:") {
			extract(line, scan, &water2Light)
		}
		if strings.Contains(line, "light-to-temperature map:") {
			extract(line, scan, &light2Temp)
		}
		if strings.Contains(line, "temperature-to-humidity map:") {
			extract(line, scan, &temp2Hum)
		}
		if strings.Contains(line, "humidity-to-location map:") {
			extract(line, scan, &hum2Loc)
		}

	}
}

func extract(line string, scan *bufio.Scanner, extract *[]conversion) {
	for line != "" {
		scan.Scan()
		line = scan.Text()
		if line != "" {
			ranges := strings.Split(line, " ")
			*extract = append(*extract, conversion{atoi(ranges[1]), atoi(ranges[0]), atoi(ranges[2])})
		}
	}
}
func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
