package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

var cardPoints map[rune]int = map[rune]int{
	'A': 14,
	'K': 13,
	'Q': 12,
	'J': 0,
	'T': 10,
	'9': 9,
	'8': 8,
	'7': 7,
	'6': 6,
	'5': 5,
	'4': 4,
	'3': 3,
	'2': 2,
	'1': 1,
}

type hand struct {
	cards          string
	scorePlay, bid int
}

func main() {
	hands := readInput()
	var sum int
	sort.Slice(hands, func(i, j int) bool {
		if hands[i].scorePlay > hands[j].scorePlay {
			return false
		} else if hands[i].scorePlay < hands[j].scorePlay {
			return true
		}
		other := hands[j].cards
		for k, card := range hands[i].cards {
			if cardPoints[card] > cardPoints[[]rune(other)[k]] {
				return false
			} else if cardPoints[card] < cardPoints[[]rune(other)[k]] {
				return true
			}
		}
		return true
	})

	for i, hand := range hands {
		sum += hand.bid * (i + 1)
	}
	fmt.Println(hands)
	fmt.Println(sum)
}

func readInput() []hand {
	file, err := os.Open("../input")
	var hands []hand = make([]hand, 0)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scan := bufio.NewScanner(file)

	for scan.Scan() {
		line := scan.Text()
		handBid := strings.Split(line, " ")
		var scoreHand map[rune]int = make(map[rune]int)
		var joker int
		for _, card := range handBid[0] {
			if card != 'J' {
				if _, ok := scoreHand[card]; ok {
					scoreHand[card]++
				} else {
					scoreHand[card] = 1
				}
			} else {
				joker++
			}
		}
		var sum float64
		if joker >= 4 {
			sum = 25
		} else {
			var bestPlay int
			var bestCard rune
			for key, value := range scoreHand {
				if value > bestPlay {
					bestPlay = value
					bestCard = key
				}
			}
			scoreHand[bestCard] = bestPlay + joker
			for _, value := range scoreHand {
				sum += math.Pow(float64(value), 2)
			}
		}

		hands = append(hands, hand{cards: handBid[0], bid: atoi(handBid[1]), scorePlay: int(sum)})
	}

	return hands
}

func atoi(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal(err)
	}
	return i
}
